; pyramid_performance make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[fontyourface][version] = "2.8"
projects[fontyourface][subdir] = "contrib"

projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.1"
projects[views_bulk_operations][subdir] = "contrib"

projects[ctools][version] = "1.3"
projects[ctools][subdir] = "contrib"

projects[context][version] = "3.0-beta7"
projects[context][subdir] = "contrib"

projects[devel][version] = "1.3"
projects[devel][subdir] = "contrib"

projects[profiler_builder][version] = "1.0"
projects[profiler_builder][subdir] = "contrib"

projects[ds][version] = "2.4"
projects[ds][subdir] = "contrib"

projects[features][version] = "2.0-rc1"
projects[features][subdir] = "contrib"

projects[entityreference][version] = "1.0"
projects[entityreference][subdir] = "contrib"

projects[fontyourface][version] = "2.8"
projects[fontyourface][subdir] = "contrib"

projects[link][version] = "1.1"
projects[link][subdir] = "contrib"

projects[title][version] = "1.0-alpha7"
projects[title][subdir] = "contrib"

projects[imce][version] = "1.7"
projects[imce][subdir] = "contrib"

projects[mongodb][version] = "1.0-rc2"
projects[mongodb][subdir] = "contrib"

projects[adaptive_image][version] = "1.4"
projects[adaptive_image][subdir] = "contrib"

projects[checklistapi][version] = "1.0-beta4"
projects[checklistapi][subdir] = "contrib"

projects[db_maintenance][version] = "1.1"
projects[db_maintenance][subdir] = "contrib"

projects[elements][version] = "1.4"
projects[elements][subdir] = "contrib"

projects[elysia_cron][version] = "2.1"
projects[elysia_cron][subdir] = "contrib"

projects[entity][version] = "1.1"
projects[entity][subdir] = "contrib"

projects[entity_autocomplete][version] = "1.0-beta3"
projects[entity_autocomplete][subdir] = "contrib"

projects[feature_set][version] = "1.1"
projects[feature_set][subdir] = "contrib"

projects[libraries][version] = "2.1"
projects[libraries][subdir] = "contrib"

projects[login_security][version] = "1.4"
projects[login_security][subdir] = "contrib"

projects[menu_block][version] = "2.3"
projects[menu_block][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[transliteration][version] = "3.1"
projects[transliteration][subdir] = "contrib"

projects[globalredirect][version] = "1.5"
projects[globalredirect][subdir] = "contrib"

projects[authcache][version] = "1.4"
projects[authcache][subdir] = "contrib"

projects[boost][version] = "1.0-beta2"
projects[boost][subdir] = "contrib"

projects[entitycache][version] = "1.1"
projects[entitycache][subdir] = "contrib"

projects[expire][version] = "1.0-beta1"
projects[expire][subdir] = "contrib"

projects[httprl][version] = "1.12"
projects[httprl][subdir] = "contrib"

projects[memcache][version] = "1.0"
projects[memcache][subdir] = "contrib"

projects[memcache_storage][version] = "1.0"
projects[memcache_storage][subdir] = "contrib"

projects[seo_checklist][version] = "4.1"
projects[seo_checklist][subdir] = "contrib"

projects[tinyids][version] = "1.0-alpha2"
projects[tinyids][subdir] = "contrib"

projects[google_analytics][version] = "1.3"
projects[google_analytics][subdir] = "contrib"

projects[ckeditor][version] = "1.13"
projects[ckeditor][subdir] = "contrib"

projects[imce_wysiwyg][version] = "1.0"
projects[imce_wysiwyg][subdir] = "contrib"

projects[superfish][version] = "1.9"
projects[superfish][subdir] = "contrib"

projects[wysiwyg][version] = "2.2"
projects[wysiwyg][subdir] = "contrib"

projects[views][version] = "3.7"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.1"
projects[views_bulk_operations][subdir] = "contrib"

